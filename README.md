## Details
Kerikuma Oshilim

## Project 
A web application built using nodejs and mongodb and licensed under the MIT license.

## Usage
Clone or download project files, run npm install and npm start.
