const express = require('express');
const path = require('path');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/project', {
    useNewUrlParser: true
});
const session = require('express-session');
const fileupload = require('express-fileupload');

//----------------------------------------------------
const Admin = mongoose.model('Admin', {
    username: String,
    password: String
});

const Page = mongoose.model('Page', {
    title: String,
    slug: String,
    image: String,
    content: String
});

const Header = mongoose.model('Header', {
    tagline: String,
    subtext: String,
    logo: String

});
var myApp = express();
myApp.use(session({
    secret: 'asecret',
    resave: false,
    saveUninitialized: true
}));


myApp.use(bodyparser.urlencoded({ extended: false }));
myApp.use(bodyparser.json());
myApp.use(fileupload());

myApp.set('views', [path.join(__dirname, 'views'),
path.join(__dirname, 'views/admin/')]);
myApp.set('view engine', 'ejs');
myApp.use(express.static(__dirname + '/public'));


//---Common area
var headersection;
Header.findOne({}, function (err, header) {
    headersection = header;
})
//----------------------------------------
myApp.get('/', function (req, res) {
    Page.find({}, function (err, pages) {
        res.render('index', { header: headersection, pages: pages });
    })

});

//-----login
myApp.get('/login', function (req, res) {
    res.render('login', { header: headersection });
});

myApp.post('/login', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;

    Admin.findOne({ username: username, password: password }, function (err, admin) {
        req.session.username = admin.username;
        req.session.userLoggedIn = true;
        res.redirect('/admin');

    });
});
myApp.get('/admin', function (req, res) {
    Admin.findOne({}, function (err, admin) {
        if (req.session.userLoggedIn) {
            res.render('admin', { header: headersection, admin: admin });
        }
        else {
            res.redirect('/login');
        }
    });
});

//----------Edit Common
myApp.get('/editcommon', function (req, res) {
    if (req.session.userLoggedIn) {
        res.render('editcommon', { header: headersection })
    }
    else {
        res.redirect('/login');
    }
});

myApp.post('/editcommon', function (req, res) {
    var tagline = req.body.tagline;
    var subtext = req.body.subtext;
    var logoName = req.files.logo.name;
    var logo = req.files.logo;
    var logopath = 'public/images/' + logoName;
    logo.mv(logopath, function (err) {
        console.log(err);
    });
    headersection.tagline = tagline;
    headersection.subtext = subtext;
    headersection.logo = logoName;
    headersection.save();
    console.log('header edited');
    res.render('admin', { header: headersection, admin: admin })


});
//------Add page
myApp.get('/addpage', function (req, res) {
    if (req.session.userLoggedIn) {
        res.render('addpage', { header: headersection })
    }
    else {
        res.redirect('/login');
    }
});

myApp.post('/addpage', function (req, res) {
    var title = req.body.title;
    var slug = req.body.slug;
    var image = req.files.image;
    var imageName = req.files.image.name;
    var imagepath = 'public/images/' + imageName;
    image.mv(imagepath, function (err) {
        console.log(err);
    })
    var content = req.body.content;
    var newPage = new Page({
        title: title,
        slug: slug,
        image: imageName,
        content: content
    });
    newPage.save().then(() => {
        res.redirect('/admin');
    })
})
//---------------
myApp.get('/viewpages', function (req, res) {
    if (req.session.userLoggedIn) {
        Page.find({}, function (err, pages) {
            res.render('viewpages', { header: headersection, pages: pages })
        })

    }
})

//---------New page
myApp.get('/:slug', function (req, res) {
    var slug = req.params.slug;
    Page.findOne({ slug: slug }, function (err, page) {
        Page.find({}, function (err, pages) {
            res.render('newpage', { header: headersection, page: page, pages: pages })
        })
    })
})






myApp.listen('8080');
console.log('Server started at port 8080...');